data "terraform_remote_state" "db" {
  backend = "s3"

  config = {
    bucket = "ey-terraform-fitec-bucket-state-demo"
    key    = "global/s3/dev/data/terraform.tfstate"
    region = "eu-west-1"
  }
}


